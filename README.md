Vncmanager
==========

Vnc helper script to manage vnc servers on a cluster.


Configuration
-------------
Make sure a file named `vnchosts` exists in the same directory as vncmanager. This file should contain a list of hostnames on which you can start vnc servers. You should be able to login to those hosts without supplying a password. For example by using an ssh `authorized_keys` file.


